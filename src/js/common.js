import "simplycountdown.js/dist/simplyCountdown.min.js";
import Choices from "choices.js/public/assets/scripts/choices";
import Typed from "typed.js";
import IMask from "imask";

window.onload = function() {
  // Title Animation
  if (document.getElementsByClassName("animated-title").length) {
    const animatedTitle = document
      .getElementById("animated-title-txt")
      .value.split(",");

    var typed = new Typed(".animated-title", {
      strings: animatedTitle,
      typeSpeed: 100,
      backSpeed: 30,
      backDelay: 3000,
      loop: true,
      showCursor: false
    });
  }

  // Password Show/Hide
  const passShow = document.getElementById("password-show");
  if (passShow) {
    const passHandler = document.getElementById(
      passShow.getAttribute("show-pass")
    );
    passShow.onmousedown = function() {
      this.classList.add("mousedown");
      passHandler.type = "text";
    };
    passShow.onmouseup = function() {
      this.classList.remove("mousedown");
      passHandler.type = "password";
    };

    passShow.ontouchstart = function() {
      this.classList.add("mousedown");
      passHandler.type = "text";
    };
    passShow.ontouchend = function() {
      this.classList.remove("mousedown");
      passHandler.type = "password";
    };
  }

  // Dropdown Toggler
  let dropdown = false;
  const profileTgl = document.getElementById("profile-tgl");
  const profileDropdown = document.getElementById("profile-dropdown");
  const bodyTag = document.getElementsByTagName("BODY")[0];

  // // Hide Restore Form
  const hideRestore = function() {
    if (document.getElementById("pass-restore")) {
      document.getElementById("pass-restore").style.display = "none";
    }
    if (document.getElementById("auth-reg")) {
      document.getElementById("auth-reg").style.display = "block";
    }
  };

  // // Dropdown Windows To Default
  const toDefault = () => {
    hideRestore();

    const tabTglsToReset =
      profileDropdown && profileDropdown.getElementsByClassName("tab__tgl");
    const tabsToReset =
      profileDropdown && profileDropdown.getElementsByClassName("tab");

    if (tabTglsToReset) {
      if (tabTglsToReset[0]) {
        tabTglsToReset[0].classList.add("active");
        tabsToReset[0].classList.add("active");
      }
      for (let i = 1; i < tabTglsToReset.length; i++) {
        tabTglsToReset[i].classList.remove("active");
        tabsToReset[i].classList.remove("active");
      }
    }
  };

  // // Reset Dropdown Forms
  const resetDropForms = function() {
    if (profileDropdown) {
      const dropdownForms = profileDropdown.getElementsByTagName("FORM");
      for (let i = 0; i < dropdownForms.length; i++) {
        dropdownForms[i].reset();
      }
    }
  };

  // // Dropdown Close
  document.onmousedown = function(event) {
    if (
      !event.target.closest(".user-profile__dropdown") &&
      !event.target.classList.contains("choices__item") &&
      dropdown
    ) {
      dropdownClose();
    }
  };

  if (document.getElementById("dropdown-close")) {
    document.getElementById("dropdown-close").onclick = dropdownClose;
  }

  function dropdownClose(event) {
    dropdown = false;
    if (profileDropdown) {
      profileDropdown.classList.remove("on");
    }
    bodyTag.classList.remove("covered");
    if (profileTgl) {
      profileTgl.classList.remove("opened");
    }

    setTimeout(() => {
      resetDropForms();
      toDefault();
    }, 250);
  }

  // // Dropdown Open
  profileTgl.onclick = function() {
    resetDropForms();
    bodyTag.classList.toggle("covered");
    if (profileDropdown) {
      profileDropdown.classList.toggle("on");
    }
    if (profileTgl) {
      profileTgl.classList.add("opened");
    }

    setTimeout(() => {
      dropdown = true;
    }, 250);
  };

  // Tab Toggle
  const tabToggles = document.getElementsByClassName("tab__tgl");
  for (let i = 0; i < tabToggles.length; i++) {
    const current = tabToggles[i];

    current.onclick = function() {
      resetDropForms();

      for (let k = 0; k < tabToggles.length; k++) {
        if (tabToggles[k] !== current) {
          tabToggles[k].classList.remove("active");
        }
      }
      current.classList.add("active");

      const tabToOpen = document.getElementById(
        "tab-" + current.getAttribute("tab")
      );
      const tabs = document.getElementsByClassName("tab");
      for (let j = 0; j < tabs.length; j++) {
        if (tabs[j] !== tabToOpen) {
          tabs[j].classList.remove("active");
        }
      }
      tabToOpen.classList.add("active");
    };
  }

  // Resotore Password
  const restoreTgl = document.getElementById("pass-restore-tgl");
  if (restoreTgl) {
    restoreTgl.onclick = function(e) {
      e.preventDefault();

      document.getElementById("auth-reg").style.display = "none";
      document.getElementById("pass-restore").style.display = "block";
    };
  }

  const backToAuth = document.getElementById("to-auth");
  if (backToAuth) {
    backToAuth.onclick = function(e) {
      e.preventDefault();
      resetDropForms();
      hideRestore();
    };
  }

  // CountDown
  const timerWrap = document.getElementById("auction-countdown");

  if (timerWrap) {
    const timerDate = {};
    timerDate.year = timerWrap.getAttribute("year");
    timerDate.month = timerWrap.getAttribute("month");
    timerDate.day = timerWrap.getAttribute("day");
    timerDate.hours = timerWrap.getAttribute("hours");
    timerDate.minutes = timerWrap.getAttribute("minutes");
    timerDate.seconds = timerWrap.getAttribute("seconds");

    for (const k in timerDate) {
      timerDate[k] = timerDate[k] && timerDate[k] > 0 ? timerDate[k] : 0;
    }

    simplyCountdown("#auction-countdown", {
      year: timerDate.year,
      month: timerDate.month,
      day: timerDate.day,
      hours: timerDate.hours,
      minutes: timerDate.minutes,
      seconds: timerDate.seconds,
      words: {
        days: "дней",
        hours: "часов",
        minutes: "минут",
        seconds: "секунд",
        pluralLetter: ""
      },
      inlineClass: "timer-countdown-inline",
      enableUtc: true,
      onEnd: function() {
        // your code
        return;
      },
      sectionClass: "timer-section",
      amountClass: "timer-amount",
      wordClass: "timer-word"
    });

    // Countdown Text Customization To Plural Form
    const daysTxtArray = document
      .getElementById("timer_days_title")
      .value.split(",");
    const hoursTxtArray = document
      .getElementById("timer_hours_title")
      .value.split(",");
    const minutesTxtArray = document
      .getElementById("timer_minutes_title")
      .value.split(",");
    const secondsTxtArray = document
      .getElementById("timer_seconds_title")
      .value.split(",");

    const daysWrap = document.getElementsByClassName("simply-days-section")[0];
    const hoursWrap = document.getElementsByClassName(
      "simply-hours-section"
    )[0];
    const minutesWrap = document.getElementsByClassName(
      "simply-minutes-section"
    )[0];
    const secondsWrap = document.getElementsByClassName(
      "simply-seconds-section"
    )[0];

    setInterval(function() {
      const daysValue = daysWrap.getElementsByClassName("timer-amount")[0];
      daysWrap.getElementsByClassName("timer-word")[0].innerText =
        daysTxtArray[declOfNum(daysValue)];

      const hoursValue = hoursWrap.getElementsByClassName("timer-amount")[0];
      hoursWrap.getElementsByClassName("timer-word")[0].innerText =
        hoursTxtArray[declOfNum(hoursValue)];

      const minutesValue = minutesWrap.getElementsByClassName(
        "timer-amount"
      )[0];
      minutesWrap.getElementsByClassName("timer-word")[0].innerText =
        minutesTxtArray[declOfNum(minutesValue)];

      const secondsValue = secondsWrap.getElementsByClassName("timer-amount")[0]
        .innerText;
      secondsWrap.getElementsByClassName("timer-word")[0].innerText =
        secondsTxtArray[declOfNum(secondsValue)];
    }, 500);

    function declOfNum(n) {
      return n % 10 == 1 && n % 100 != 11
        ? 0
        : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20)
        ? 1
        : 2;
    }
  }

  // Selects - Choices.js
  if (document.getElementsByClassName("choice-select").length) {
    const choices = new Choices(".choice-select");
  }

  // Lot Toggle
  const lotTgl = document.getElementsByClassName("participate-btn");
  for (let i = 0; i < lotTgl.length; i++) {
    lotTgl[i].onclick = function() {
      const lotList = document.getElementsByClassName("lot-item");
      const lotWrap = lotTgl[i].closest(".lot-item");
      lotWrap.classList.toggle("opened");
      lotWrap.getElementsByTagName("FORM")[0].reset();

      if (lotTgl[i].innerText === lotTgl[i].getAttribute("opened")) {
        lotTgl[i].innerText = lotTgl[i].getAttribute("closed");
      } else {
        lotTgl[i].innerText = lotTgl[i].getAttribute("opened");
      }

      for (let j = 0; j < lotList.length; j++) {
        if (lotList[j] !== lotWrap) {
          lotList[j].classList.remove("opened");
          lotList[j].getElementsByClassName(
            "participate-btn"
          )[0].innerText = lotList[j]
            .getElementsByClassName("participate-btn")[0]
            .getAttribute("closed");
        }
      }
    };
  }

  // Price Control
  const priceReduce = document.getElementsByClassName("reduce-100");
  const priceIncrease = document.getElementsByClassName("increase-100");

  for (let i = 0; i < priceIncrease.length; i++) {
    priceReduce[i].onclick = function() {
      const parent = priceReduce[i].parentElement;
      const priceInput = parent.getElementsByClassName("lot-price-input")[0];
      const priceHandler = parent.getElementsByClassName("price-num")[0];
      const minPrice = priceHandler.getAttribute("min");
      const price = priceHandler.innerText;

      if (
        Number(price) > Number(minPrice) &&
        !priceReduce[i].classList.contains("disabled")
      ) {
        priceHandler.innerText = Number(price) - 100;
        priceInput.value = Number(price) - 100;
      } else {
        priceReduce[i].classList.add("disabled");
      }
    };
    priceIncrease[i].onclick = function() {
      const parent = priceReduce[i].parentElement;
      const priceInput = parent.getElementsByClassName("lot-price-input")[0];
      const priceHandler = parent.getElementsByClassName("price-num")[0];
      const price = priceHandler.innerText;

      priceReduce[i].classList.remove("disabled");
      priceHandler.innerText = Number(price) + 100;
      priceInput.value = Number(price) + 100;
    };
  }

  // Phone Field Manupulating
  const phoneFields = document.getElementsByClassName("phone-field");
  for (let i = 0; i < phoneFields.length; i++) {
    const el = phoneFields[i];

    const maskOptions = {
      mask: "+{996} 000 00 00 00",
      lazy: false,
      placeholderChar: "*",
      maxLength: 13
    };
    const mask = new IMask(el, maskOptions);

    el.onchange = function() {
      if (el.value.includes("*")) {
        el.value = "";
      }
    };
  }

  // Check Required Fields For Changing
  const changeCheck = function(formSubmit, parentForm, submit = false) {
    const requriedFormFields = parentForm.getElementsByClassName(
      "required-item"
    );
    let errorCount = requriedFormFields.length;

    for (let j = 0; j < requriedFormFields.length; j++) {
      const formEl = requriedFormFields[j];
      formEl.classList.remove("error-item");

      if (formEl.type === "checkbox" && formEl.checked) {
        errorCount--;
      } else if (
        (formEl.type === "text" ||
          formEl.type === "password" ||
          formEl.type === "select-one") &&
        formEl.value !== ""
      ) {
        errorCount--;
      } else {
        if (submit) {
          formEl.classList.add("error-item");
        }
      }
    }

    if (errorCount === 0) {
      formSubmit.removeAttribute("disabled");
    } else {
      formSubmit.setAttribute("disabled", "disabled");
    }
  };

  // Requried Form Items On Change
  const requriedFields = document.getElementsByClassName("required-item");
  for (let i = 0; i < requriedFields.length; i++) {
    const el = requriedFields[i];
    const parentForm = el.closest("form");
    const formSubmit = parentForm.getElementsByTagName("BUTTON")[0];

    if (el.type === "checkbox" || el.type === "select-one") {
      el.onchange = function() {
        changeCheck(formSubmit, parentForm);
      };
    } else {
      el.onkeyup = function() {
        changeCheck(formSubmit, parentForm);
      };
    }
  }

  // Form Submit Validdation
  const formList = document.getElementsByTagName("FORM");
  for (let i = 0; i < formList.length; i++) {
    const form = formList[i];
    const formSubmit = form.getElementsByTagName("BUTTON")[0];

    form.onsubmit = function() {
      changeCheck(formSubmit, form, true);
    };
  }

  // // Footer Year
  // const currentDate = new Date();
  // document.getElementById("footer-year").innerText = currentDate.getFullYear();
};

// Notifications
document.notification = function(message) {
  const notification = document.getElementById("notification-wrap");

  notification.getElementsByClassName(
    "notification__txt"
  )[0].innerText = message;
  notification.classList.add("shown");

  setTimeout(() => {
    notification.classList.remove("shown");
  }, 2000);
};
